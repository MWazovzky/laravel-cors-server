<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

## Laravel based API server-template

### Laravel CORS
Laravel by default blocks cross-origin HTTP request.    
See [CORS](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS).

Cross-site requests for specified domains are allowed via 'cors' middleware ```app/Http/Middleware/Cors.php```.   
White-listed domains are defined in ```config/cors.php```.