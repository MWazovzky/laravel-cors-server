<?php

return [

    /*
    /--------------------------------------------------------------------------
    / Allowed domains 
    /--------------------------------------------------------------------------
    /
    / Here you may specify domains that are allowed 
    / to make cross-site API request
    /
   */

    'domains' => [
        'http://localhost:8080',
    ]
    
];
